sim/
- AA and AA-mapped simulations
./Hexane/AA_single_mol/Prod_run/Mapped/cghex1.cat30.renum.trr -- 13ns*30 runs
./Hexane/AA_single_mol/Prod_run/Mapped/traj_intra.dat -- corresponding traj along internal coordinates

DPCA/
- clustering of the AA-mapped trajectory along the internal coordinates, 7 clusters found
./Dtraj_for_DPCA.ipynb -- transform the traj for processing: the internal coordinate space must be shifted and normalized to zero mean and unit total variance.
./mu.dat -- mean values of internal coords
./sd.dat -- standard deviation of internal coord dists
- x' = (x-mu)/sd for each coord
./commands.sh -- performs the clustering using the ../../soft/Clustering-0.12/ program
./microstates -- trajectory projected onto the 7 clusters
./Vis_Clusters.ipynb -- visualize the clusters + reduction of cluster numbers (and corresponding mappings)
- first, symmetric redundencies were removed, resulting in 5 clusters
./microstates_renum2.dat -- trajectory projected onto the 5 clusters
- then, the number of clusters were systematically reduced
./microstates_4states.dat
./microstates_3states.dat
./microstates_2states.dat -- (see ./Vis_Clusters.ipynb for details)

Assign/
./Xstate/ -- data for representation with X clusters
./Xstate/Assign_Clusters.ipynb - checks that the assignment scheme worked (i.e., largely matched the clustering analysis) and prints our drop files to parse the Gromacs trajectories
./Xstate/run_drop.sh - parses the original traj into sub-trajectories for each cluster

CGFF/
- All FM calculations are using the BOCS software: ../../soft/BOCS-master/
./full_enemble/ -- regular FMing of bond and angle from the full trajectory
./Xstates/ -- does FMing on the sub-ensembles for each representation
./Xstates/cf_angle_dist_full.agr -- compares the resulting angle distributions for each FF using xmgrace
./Xstates/statei_wbond/ -- FMing calc and sim of resulting potential for representation of X total states and for the ith cluster in particular
./Xstates/statei_wbond/edit/table_xx.xvg -- resulting table files from FMing of this sub-ensemble
./Xstates/statei_wbond/cgsim/ -- sim with the resulting FF

