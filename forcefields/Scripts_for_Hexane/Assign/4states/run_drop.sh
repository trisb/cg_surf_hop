Nstates=4

for i in `seq 0 $((${Nstates}-1))`;
do
    trjconv -f ../../sim/AA_single_mol/Prod_run/Mapped/cghex1.cat30.renum.trr -s ../../sim/AA_single_mol/Prod_run/Mapped/cghex1.tpr -drop drop_state${i}.xvg -dropunder 0.5 -o cghex1.state${i}.trr -force <<-EOF
0
EOF

done

