#!/bin/bash

#####################################################################

#System Specific
sys="cghex1"

blen=0.25
tol=2.0

######################################################################

#Directories
bin="/data/isilon/rudzinski/soft_backup/gromacs-4.5.3-openmpi-dp/bin"
suff="_d"

######################################################################

#Executables
g_rdf="${bin}/g_rdf${suff}"
g_bond="${bin}/g_bond${suff}"
g_angle="${bin}/g_angle${suff}"
g_rmsd="${bin}/g_rmsdist${suff}"
g_rmsf="${bin}/g_rmsf${suff}"

#g_rdf="g_rdf4.5.3"
#g_bond="g_bond4.5.3"
#g_angle="g_angle4.5.3"
#g_rmsd="g_rmsdist4.5.3"
#g_rmsf="g_rmsf4.5.3"

######################################################################

#File Names
tpr="${sys}.tpr"
trr="../${sys}.state0.trr"
ndx="index.ndx"
native="${sys}.gro"

######################################################################

#Calculate Distributions

#bonds
frames=130010

$g_bond -f ${trr} -s ${tpr} -blen ${blen} -tol ${tol} -n ${ndx} -d bonds_1_trace.xvg -o bonds_1_dist.xvg <<-EOF
       0
EOF

#tail -n ${frames} bonds_1_trace.xvg &> tmp.xvg
#mv tmp.xvg bonds_1_trace.xvg

$g_bond -f ${trr} -s ${tpr} -blen ${blen} -tol ${tol} -n ${ndx} -d bonds_2_trace.xvg -o bonds_2_dist.xvg <<-EOF
       1
EOF

#tail -n ${frames} bonds_2_trace.xvg &> tmp.xvg
#mv tmp.xvg bonds_2_trace.xvg

$g_angle -f ${trr} -n ${ndx} -ov angles_1_trace.xvg -od angles_1_dist.xvg <<-EOF
       2
EOF

#tail -n ${frames} angles_1_trace.xvg &> tmp.xvg
#mv tmp.xvg angles_1_trace.xvg
