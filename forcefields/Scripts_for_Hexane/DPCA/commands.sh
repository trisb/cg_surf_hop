Nproc=16
clustering="/data/isilon/rudzinski/soft_backup/Clustering-0.12_static/clustering"

ln -s dtraj_tica3D.dat coords

${clustering} density -f coords -R 0.025 0.05 0.1 0.2 0.3 0.5 -p pop -d fe -n ${Nproc} -v 
# note use 0.0025
R=0.1
pop_min=200
dFE=0.1

#&& 
ln -s fe_0.100000 fe 

#&& 

${clustering} density -f coords -r ${R} -D fe -b nn -n ${Nproc} -v

#&& 

${clustering} density -f coords -r ${R} -D fe -B nn -T 0. $dFE -o clust -n ${Nproc} -v

${clustering} network -p ${pop_min} --step $dFE -v && ${clustering} density -f coords -i network_end_node_traj.dat -r ${R} -D fe -B nn -o microstates -n ${Nproc} -v

#${clustering} mpp -i microstates -D fe -l 10 -v --concat-nframes 13001
