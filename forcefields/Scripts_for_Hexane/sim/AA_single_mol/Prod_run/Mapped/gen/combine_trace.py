import numpy as np

b1 = np.genfromtxt('bonds_1_trace.xvg')[:,1]
b2 = np.genfromtxt('bonds_2_trace.xvg')[:,1]
a1 = np.genfromtxt('angles_1_trace.xvg')[:,1]
t = np.arange(a1.shape[0])

traj = np.vstack((t,b1,b2,a1))

np.savetxt('traj_intra.dat',traj.T)
