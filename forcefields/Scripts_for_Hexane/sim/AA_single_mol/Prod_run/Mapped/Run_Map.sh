# replace T in each mdp file
sys='hex1'
run0=11
Nruns=20
rand=( 934 45754 456 23452 487 876 345 87 345 9832 236 8346 472 98235 23 56 23 872 123 54 812 9532 123 984 619 391 857 )

for run in $(seq ${run0} $((${run0}+${Nruns}-1)))
do
    cd run${run}
    ./CGMap.sh
    ./calc_dist.sh
    python combine_trace.py
    cd ../
done

