#!/bin/bash

#EXE="/data/isilon/rudzinski/soft_backup/Gromacs_Tables_07.02.13/gmx4_tables-intracut"
EXE="/usr/data/rudzinski/soft_backup2/Gromacs_Tables_07.02.13/gmx4_tables-intracut"

INPUT="f_forces.BondStretch.total.Bonds.dat"
OUTPUT="table_b0.xvg"
TYPE="bond"
BASIS="linear"
RMIN="0.0"
RMAX="0.600"
SLOPE="50000.0"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMIN $RMAX $SLOPE

INPUT="f_forces.Angle.total.Angles.dat"
OUTPUT="table_a0.xvg"
TYPE="angle"
BASIS="linear"
SLOPE="0.25"
$EXE $INPUT $OUTPUT $TYPE $BASIS $SLOPE

