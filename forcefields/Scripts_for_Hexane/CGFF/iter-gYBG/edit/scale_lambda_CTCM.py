#!/usr/bin/python
from math import sqrt
from math import fabs

xold = []
fold = []
xnew = []
fnew = []
lam = 1.0
filename = open("lam.txt","r")
if filename:
   for line in filename:
       line = line.strip()
       if ( float(line.split()[0]) < float(lam) ):
         lam = line.split()[0]

filename = open("table_old.xvg","r")
if filename:
   for line in filename:
       line = line.strip()
       xold.append(line.split()[0])
       fold.append(line.split()[6])

filename = open("f_CTCM.dat","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    xnew.append(line.split()[0])
    fnew.append(line.split()[1])     

N = len(xold)
for i in range(0,N):
  if ( fabs(float(xold[i]) - float(xnew[0])) < 1e-6 ):
    iold = i
    break

N = len(xnew)
for i in range(0,N):
    print str(xnew[i])+" "+str( float(fold[iold]) + float(lam)*(float(fnew[i]) - float(fold[iold])) )
    iold += 1

