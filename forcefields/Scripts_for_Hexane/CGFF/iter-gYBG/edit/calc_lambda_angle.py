#!/usr/bin/python
from math import sqrt
from math import fabs

berr = []
d2berr = 0.00
bAA = []
d2bAA = 0.00

lammin = 0.05
lamold = 1.00
lam = 1.00
kT = 2.4777

filename = open("lam_min.txt","r")
if filename:
   for line in filename:
       line = line.strip()
       lammin = line.split()[0]

filename = open("lam_old.txt","r")
if filename:
   for line in filename:
       line = line.strip()
       if ( float(line.split()[0]) < float(lamold) ):
         lamold = line.split()[0]

filename = open("../b_soln_err.Angles.dat","r")
if filename:
   for line in filename:
       line = line.strip()
       berr.append(line.split()[0])

filename = open("../bAA.Angles.dat","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    bAA.append(line.split()[0])

N = len(bAA)
for i in range(0,N):
  d2berr += float(berr[i])*float(berr[i])
  d2bAA += float(bAA[i])*float(bAA[i]) 

d2berr = sqrt(d2berr)
d2bAA  = sqrt(d2bAA)
if ( (d2berr/d2bAA) <= 1.00 ):
  lam = 1.00 - (d2berr/d2bAA)
else:
  if ( fabs(float(lamold)-float(lammin)) < 1e-6 ):
    lam = float(lammin)*(d2berr/d2bAA)
    fp = open('lam_min_tmp.txt', 'w+')
    fp.write(str(lam)+'\n')
  else:
    lam = float(lammin)

if ( float(lam) > 1.5*float(lamold) ):
  lam = float(1.5*float(lamold))

if ( float(lam) < float(lammin) ):
  lam = float(lammin)

fp = open('lam.txt', 'a')
fp.write(str(lam)+'\n')

fp = open('error.txt', 'a')
fp.write(str(float(d2berr/d2bAA))+'\n')

