#!/bin/bash

EXE="/gpfs/work/jfr148/pkg/Gromacs_Tables_07.02.13/gmx4_tables-intracut"

INPUT="f_CMCM_lambda.dat"
OUTPUT="table_CM_CM.xvg"
TYPE="nb"
BASIS="Bspline"
RMAX="3.00"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX

INPUT="f_CTCM_lambda.dat"
OUTPUT="table.xvg"
TYPE="nb"
BASIS="Bspline"
RMAX="3.00"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX

INPUT="f_CTCT_lambda.dat"
OUTPUT="table_CT_CT.xvg"
TYPE="nb"
BASIS="Bspline"
RMAX="3.00"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX

INPUT="f_bond_lambda.dat"
OUTPUT="table_b0.xvg"
TYPE="bond"
BASIS="linear"
RMIN="0.0"
RMAX="0.800"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMIN $RMAX

INPUT="f_angle_lambda.dat"
OUTPUT="table_a0.xvg"
TYPE="angle"
BASIS="linear"
$EXE $INPUT $OUTPUT $TYPE $BASIS

