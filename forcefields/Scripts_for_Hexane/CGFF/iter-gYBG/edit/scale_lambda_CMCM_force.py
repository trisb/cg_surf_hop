#!/usr/bin/python
from math import sqrt
from math import fabs

xold = []
fold = []
xnew = []
fnew = []
lam = 1.00
filename = open("lam.txt","r")
if filename:
   for line in filename:
       line = line.strip()
       if ( float(line.split()[0]) < float(lam) ):
         lam = line.split()[0]

filename = open("../force_imin1_check.CMCM.dat","r")
if filename:
   for line in filename:
       line = line.strip()
       fold.append(line.split()[0])

filename = open("../force_i.CMCM.dat","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    fnew.append(line.split()[0])     


fp = open('../force_i_scale.CMCM.dat', 'w+')

fp.write(str(fnew[0])+'\n')
fp.write(str(fnew[1])+'\n')

N = len(fnew)
for i in range(2,N):
  fp.write(str(float(fold[i])+float(lam)*(float(fnew[i])-float(fold[i])))+'\n')

