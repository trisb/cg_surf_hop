#!/bin/bash

#####################################################################

#System Specific
sys="cgALA4"

blen=0.4
tol=0.75

######################################################################

#Directories
bin="/data/isilon/rudzinski/soft_backup/gromacs-4.5.3-openmpi-dp/bin"
suff="_d"

######################################################################

#Executables
g_rdf="${bin}/g_rdf${suff}"
g_bond="${bin}/g_bond${suff}"
g_angle="${bin}/g_angle${suff}"
g_rmsd="${bin}/g_rmsdist${suff}"
g_rmsf="${bin}/g_rmsf${suff}"
g_gyrate="${bin}/g_gyrate${suff}"

######################################################################

#File Names
tpr="../${sys}.tpr"
trr="../${sys}.cat10.renum.trr"
ndx="index_dist.ndx"
native="${sys}.gro"

######################################################################

#Calculate Distributions

#bonds

${g_bond} -f ${trr} -s ${tpr} -xvg none -blen ${blen} -tol ${tol} -n ${ndx} -o bonds.xvg <<-EOF
       1
EOF

${g_bond} -f ${trr} -s ${tpr} -xvg none -blen 0.7 -tol ${tol} -n ${ndx} -d distance_1-4.xvg -o bonds_1-4.xvg <<-EOF
       3
EOF

#angles

${g_angle} -binwidth 0.5 -xvg none -f ${trr} -n ${ndx} -od angles.xvg <<-EOF
       4
EOF

#dihedrals

${g_angle} -binwidth 2.0 -xvg none -f ${trr} -n ${ndx} -type dihedral -od dihedrals.xvg -ov dih_trace.xvg <<-EOF
       5
EOF

tail -n +10 dihedrals.xvg &> dihedrals_tmp.xvg

mv dihedrals_tmp.xvg dihedrals.xvg

${g_gyrate} -f ${trr} -s ${tpr} -xvg none -n ${ndx} -o Rg.xvg <<-EOF
       0
EOF
