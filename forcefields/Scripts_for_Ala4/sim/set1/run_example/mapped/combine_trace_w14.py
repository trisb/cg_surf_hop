import numpy as np

a = np.genfromtxt('angles_trace.xvg')[:,1]
d = np.genfromtxt('dihedrals_trace.xvg')[:,1]
b = np.genfromtxt('bonds_1-4_trace.xvg')[:,1]
t = np.arange(a.shape[0])

traj = np.vstack((t,a,d,b))

np.savetxt('traj_intra_w14.dat',traj.T)
