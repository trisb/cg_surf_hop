#!/bin/bash
# $1 = trr filename 
# $2 = system.tag
# All itp files are needed

# Set begin time and end time

i="ALA4"

cg_top="cg$i.top"
cg_mdp="cg$i.mdp"
cg_gro="cg$i.gro"
cg_tpr="cg$i.tpr"
aa_tpr="../nowater/ALA4_nowater.tpr"
map_top="map$i.top"
cg_trr="cg$i.trr"
aa_trr="../nowater/ALA4_nowater_pbcwhole.trr"
out_trr=${aa_trr}
log_file="temptrr.log"


#/gpfs/work/jfr148/pkg/gromacs-4.5.3-dp/bin/trjconv-xj_sn-4 -f $aa_trr -force -pbc whole -s $aa_tpr -o $out_trr >& $log_file <<-EOF
#	1
#EOF

#/gpfs/work/jfr148/pkg/gromacs-4.5.3/bin/grompp-xj_sn-4 -p $cg_top -f $cg_mdp -c $cg_gro -o $cg_tpr

/usr/data/rudzinski/soft_backup2/CGMap-gmx-4.5.3-dp/src/CGMap-gmx -f $out_trr -p $map_top -s $cg_tpr -o $cg_trr
