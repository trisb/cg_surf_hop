import numpy as np

a = np.genfromtxt('angles_trace.xvg')[:,1]
d = np.genfromtxt('dihedrals_trace.xvg')[:,1]
t = np.arange(a.shape[0])

traj = np.vstack((t,a,d))

np.savetxt('traj_intra.dat',traj.T)
