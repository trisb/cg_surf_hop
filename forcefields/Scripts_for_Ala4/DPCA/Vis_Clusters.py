
# coding: utf-8

# TICA and clustering with CN- of 80:20 KA sims
# ====

# In[1]:

import pyemma
pyemma.__version__

import numpy as np
# In[2]:

import os
#get_ipython().magic(u'pylab inline')
#matplotlib.rcParams.update({'font.size': 12})


# In[3]:

import pyemma.coordinates as coor
import pyemma.msm as msm
import pyemma.plots as mplt
from collections import Counter
import mdtraj as md
#from CN_functions import *
#from gen_dtraj_functions import *


# Some useful inhouse functions
# ------

# In[4]:

#from plot_functions import *
import pickle


# Read in the data
# ------

# In[5]:

tica_lags = [1] 
tica_dim = 3
clust_dim = 3
nclust_max = 1000
dtraj = np.genfromtxt('dtraj_tica3D.dat')
# Y = []
# tica_corr = []
# for lag in tica_lags:
#     Y.append(np.load('../TICA/Y_ticadim-'+str(tica_dim)+'_ticalag-'+str(lag)+'.npy'))
#     tica_corr.append(np.load('../TICA/tica_corr_lag-'+str(lag)+'.npy'))
    #  clustering
    #with open('../TICA/clustering_regspaceB_ticadim-'+str(tica_dim)+'_ticalag-'+str(lag)+'_nclust-'+str(nclust_max)+'_clustdim-'+str(clust_dim)+'.pkl', 'rb') as f:
    #    clustering = pickle.load(f)
#Y_pca = np.load('Y_B_nclust-200_pcadim-10_clustdim-3.npy')


# In[6]:

np.save('tica_lag',tica_lags[0])
np.save('tica_dim',tica_dim)
np.save('clust_dim',clust_dim)
np.save('nclust_max',nclust_max)


# In[7]:

microstates = np.genfromtxt('microstates')


# In[8]:

# from copy import deepcopy
# tica_lag = 1 #tica_lags[0]
# N_dim = Y[0][0].shape[1]
# Y_trim = []
# for traj in range(len(Y[0])):
#     Y_trim.append( Y[0][traj][::tica_lag,0:N_dim] )


# In[9]:

np.unique(microstates).shape


# In[10]:

state_pos = [[] for dim in range(clust_dim)]
for state in np.unique(microstates):
    state_frames = np.where(microstates==state)[0]
    for dim in range(clust_dim):
        state_pos[dim].append( np.mean( dtraj[:,dim][state_frames] ) )


# In[11]:

#for lag in range(len(tica_lags)):
#    for dim in range(clust_dim-1):
#        fig, ax = mplt.plot_free_energy(dtraj[:,dim+1], dtraj[:,0],cmap='terrain')
#        for state in range(len(np.unique(microstates))):
#            plot(state_pos[dim+1][state],state_pos[0][state], linewidth=0, marker='o', markersize=4, color='red') #, markerfacecolor='none')
#             plt.xlim([-1,1])
#             plt.ylim([-1,1])
#        if dim == 0:
#            plt.savefig('fig_bond-bond.eps', format='eps', dpi=800)
#        else:
#            plt.savefig('fig_bond-angle.eps', format='eps', dpi=800)
        # plt.savefig('fig_P-WCN-BB-'+str(feat)+'_'+str(N_mss[nstate])+'HstateHMM.eps', format='eps', dpi=800)
#        plt.show()


# In[12]:

len(np.unique(microstates))
np.unique(microstates).astype(int)


# In[13]:

Q_cut = [0.100,0.200,0.500]
for cut in Q_cut:
    print 'cut is '+str(cut)
    microstates = np.genfromtxt('mpp_traj_'+"{0:.3f}".format(cut)+'.dat')
    print 'nclust = '+str(np.unique(microstates).shape[0])
    #
    state_pos = [[] for dim in range(clust_dim)]
    for state in np.unique(microstates):
        state_frames = np.where(microstates==state)[0]
        for dim in range(clust_dim):
            state_pos[dim].append( np.mean( dtraj[:,dim][state_frames] ) )
    dim = 0
#    for lag in range(len(tica_lags)):
#        fig, ax = mplt.plot_free_energy(dtraj[:,1], dtraj[:,0],cmap='terrain')
#        for state in range(len(np.unique(microstates))):
            #plot(state_pos[0][state],state_pos[dim+1][state], linewidth=0, marker='o', markersize=4, color='red') #, markerfacecolor='none')
#            plot(state_pos[1][state],state_pos[0][state], linewidth=0, marker='o', markersize=15, color='red',alpha=0.5) #, markerfacecolor='none')

    #         plt.xlim([0.2,0.6])
    #         plt.ylim([-1.5,-0.5])
#    plt.show()


# In[ ]:




# In[14]:

# Let's see what these clusters correspond to


# In[15]:

# dtrajs = []
# path = '../traj/vals.txt'
# dtrajs.append(np.genfromtxt(path)[:,1:])


# In[16]:

# concat the trajs
# dtrajs_concat = np.vstack(dtrajs)


# In[17]:

# use the highest res rep
cut = 0.500
microstates = np.genfromtxt('mpp_traj_'+"{0:.3f}".format(cut)+'.dat')
#microstates = np.genfromtxt('microstates')


# In[18]:

clust_num_OLD = np.unique(microstates).astype(int)
clust_num_OLD


# In[19]:

from copy import deepcopy
microstates_renum = deepcopy(microstates)
for state_ind,state in enumerate(clust_num_OLD):
    states = np.where(microstates==state)[0]
    microstates_renum[states] = state_ind


# In[20]:

np.savetxt('mpp_traj_'+"{0:.3f}".format(cut)+'_renum.dat',microstates_renum.astype(int),fmt='%d')
#np.savetxt('microstates_renum.dat',microstates_renum.astype(int),fmt='%d')


# In[21]:

np.unique(microstates_renum)


# In[22]:

mu = np.genfromtxt('mu.dat')
sd = np.genfromtxt('sd.dat')


# In[23]:

#! dtraj is already shifted
# from copy import deepcopy
# dtraj_shift = deepcopy(dtraj)
# for traj in range(len(dtraj)):
#     dtraj_shift[traj] = (dtraj[traj]-mu)/sd


# In[24]:

mu_state = []
sd_state = []
for state in np.unique(microstates_renum).astype(int):
    state_frs = np.where(microstates_renum==state)[0]
    mu_state.append(float(len(state_frs)) / len(microstates_renum))
    sd_state.append(np.sqrt(np.var(dtraj[state_frs],axis=0)))


# In[25]:

print np.array(mu_state)
print np.array(state_pos).T


# In[26]:

np.savetxt('mu_state.dat',mu_state,fmt='%f')
np.savetxt('sd_state.dat',sd_state,fmt='%f')


# In[27]:

sd.astype(float)


# In[ ]:




# In[28]:

N_feat = dtraj.shape[1]
print N_feat
N_clust = len(np.unique(microstates))
print N_clust


# In[29]:

# let's use the original trajectory
dtraj_OG = np.load('dtraj_OG.npy')
N_feat = dtraj_OG.shape[1]
print N_feat
feat_dist = [[] for state in range(N_clust)]
for state_ind,state in enumerate(np.unique(microstates)):
    state_frs = np.where(microstates==state)[0]
    for feat_ind in range(N_feat):
        feat_dist[state_ind].append(dtraj_OG[state_frs,feat_ind])
feat_dist = np.array(feat_dist)


# In[30]:

# sort the clusters in order of the first TICA component
import operator
state_pos = []
for state in np.unique(microstates).astype(int):
    state_frames = np.where(microstates==state)[0]
    state_pos.append( np.mean( dtraj[:,0][state_frames] ) )

state_prop = []
for state_ind,state in enumerate(np.unique(microstates).astype(int)):
    state_prop.append([])
    state_prop[state_ind].append(state_pos[state_ind])
    state_prop[state_ind].append(state_ind)
sorted_list = sorted(state_prop, key=operator.itemgetter(0), reverse=False)
state_map = np.array(sorted_list)[:,-1].astype(int)
state_map_r = np.zeros(state_map.shape[0]).astype(int)
for state_ind,state in enumerate(np.unique(microstates).astype(int)):
    state_map_r[state_ind] = np.where(state_map==state_ind)[0]
# models = np.array(models)[model_map]
# model_nm = np.array(model_nm)[model_map]
# models_float = np.array(models_float)[model_map]
# print models
# print model_nm
# print models_float
#models_float
print state_map_r
np.save('state_map_r',state_map_r)
np.save('state_map',state_map)


# In[31]:


# In[32]:

# print 'cut is '+str(cut)
microstates = np.genfromtxt('mpp_traj_'+"{0:.3f}".format(cut)+'.dat')
#microstates = np.genfromtxt('microstates')
print 'nclust = '+str(np.unique(microstates).shape[0])
#
state_pos = [[] for dim in range(clust_dim)]
for state in np.unique(microstates).astype(int):
    state_frames = np.where(microstates==state)[0]
    for dim in range(clust_dim):
        state_pos[dim].append( np.mean( dtraj[:,dim][state_frames] ) )
dim = 0
#for lag in range(len(tica_lags)):
#    fig, ax = mplt.plot_free_energy(dtraj[:,0], dtraj[:,1],cmap='terrain')
#    for state in range(len(np.unique(microstates))):
        #plot(state_pos[0][state],state_pos[dim+1][state], linewidth=0, marker='o', markersize=4, color='red') #, markerfacecolor='none')
#        plot(state_pos[0][state],state_pos[dim+1][state], linewidth=0, marker='o', markersize=15, color=mss_colors[state],alpha=0.5) #, markerfacecolor='none')
#        ax.annotate(state, (state_pos[0][state]-0.06,state_pos[dim+1][state]-0.06))

#         plt.xlim([0.2,0.6])
#         plt.ylim([-1.5,-0.5])
#plt.show()


# In[33]:

print 'nclust = '+str(np.unique(microstates).shape[0])
#
state_pos = [[] for dim in range(clust_dim)]
for state in np.unique(microstates).astype(int):
    state_frames = np.where(microstates==state)[0]
    for dim in range(clust_dim):
        state_pos[dim].append( np.mean( dtraj[:,dim][state_frames] ) )
dim = 0
#for lag in range(len(tica_lags)):
#    fig, ax = mplt.plot_free_energy(dtraj[:,0], dtraj[:,1],cmap='terrain')
#    for state in range(len(np.unique(microstates))):
        #plot(state_pos[0][state],state_pos[dim+1][state], linewidth=0, marker='o', markersize=4, color='red') #, markerfacecolor='none')
#        plot(state_pos[0][state],state_pos[dim+1][state], linewidth=0, marker='o', markersize=15, color=mss_colors[state_map_r[state]],alpha=0.5) #, markerfacecolor='none')
#        ax.annotate(state_map_r[state]+1, (state_pos[0][state]-0.06,state_pos[dim+1][state]-0.06))

#         plt.xlim([0.2,0.6])
#         plt.ylim([-1.5,-0.5])
#plt.show()


# In[34]:

np.save('state_pos',state_pos)
#np.save('state_colors',mss_colors)


# In[35]:

np.array(state_pos)[:,0]


# In[36]:

mu = np.genfromtxt('mu.dat')
sd = np.genfromtxt('sd.dat')


# In[37]:

state_map_r


# In[38]:

# everything by hand for Figs
feat_ind = 0
#fig = plt.figure(figsize=(10,6))
#ax = fig.add_subplot(111)
#for state_ind in range(N_clust):
#    label = 'state '+str(state_map_r[state_ind]+1)
#    tmp = plt.hist( feat_dist[state_ind][feat_ind], bins=25, normed=True, color=mss_colors[state_map_r[state_ind]], alpha=0.5, label=label )
#    plt.plot(np.mean(feat_dist[state_ind][feat_ind])*np.ones(10),np.linspace(0,0.1,10),color=mss_colors[state_map_r[state_ind]],linewidth=2)

#ax.spines['left'].set_linewidth(3.0)
#ax.spines['right'].set_linewidth(3.0)
#ax.spines['top'].set_linewidth(3.0)
#ax.spines['bottom'].set_linewidth(3.0)

# legend
#plt.legend()
#legend = plt.legend(fontsize='26',loc='upper center', bbox_to_anchor=(1.2, 0.98))
#frame = legend.get_frame()
#frame.set_linewidth('3.0')
#ltext = legend.get_texts()
#plt.setp(ltext, fontweight='normal')

# plt.ylabel(r'${\rm P}({\rm WCN})$',fontsize='30',fontweight='normal')
# plt.xlabel(r'$E_{\rm LJ-SR:CN-H}$',fontsize='30',labelpad=10)

# plt.yticks(np.arange(10)*0.1,fontsize='20')
# plt.xticks(np.arange(8),fontsize='20')
# ax.tick_params(axis='both', which='major', pad=5)

# plt.ylim([0.,0.1])
# plt.xlim([-250,10])

# plt.savefig('fig_P-WCN-BB-'+str(feat)+'_'+str(N_mss[nstate])+'HstateHMM.svg', format='svg', dpi=800)
# plt.savefig('fig_P-WCN-BB-'+str(feat)+'_'+str(N_mss[nstate])+'HstateHMM.eps', format='eps', dpi=800)
    
#plt.show()


# In[39]:

# everything by hand for Figs
# In[ ]:




# In[40]:

# remove symmetrically identical states


# In[41]:

microstates_renum2 = deepcopy(microstates_renum)
# microstates_renum2[np.where(microstates_renum2==3)[0]] = 2
# microstates_renum2[np.where(microstates_renum2==4)[0]] = 3
# microstates_renum2[np.where(microstates_renum2==5)[0]] = 4
# microstates_renum2[np.where(microstates_renum2==6)[0]] = 4


# In[42]:

np.savetxt('microstates_renum2.dat',microstates_renum2.astype(int),fmt='%d')


# In[43]:

for state in range(np.unique(microstates_renum2).shape[0]):
    print 'state '+str(state)+' has pop '+str(float(len(np.where(microstates_renum2==state)[0]))/len(microstates_renum2))


# In[44]:

N_clust = np.unique(microstates_renum2).shape[0]
N_feat = dtraj.shape[1]
feat_dist = [[] for state in range(N_clust)]
for state_ind,state in enumerate(np.unique(microstates_renum2)):
    state_frs = np.where(microstates_renum2==state)[0]
    for feat_ind in range(N_feat):
        feat_dist[state_ind].append(dtraj[state_frs,feat_ind])
feat_dist = np.array(feat_dist)


# In[45]:

# everything by hand for Figs
# In[46]:

# In[ ]:




# In[48]:

# Now, let's combine states directly from these definitions


# In[49]:

state_pos = np.array(state_pos)


# In[50]:

microstates_2states = deepcopy(microstates_renum2)
microstates_2states[np.where(microstates_renum2==2)[0]] = 1
microstates_2states[np.where(microstates_renum2==3)[0]] = 0
np.savetxt('microstates_2states.dat',microstates_2states.astype(int),fmt='%d')


# In[ ]:




# In[ ]:




# In[ ]:




# In[62]:




# In[55]:




# In[ ]:




# In[56]:




# In[57]:




# In[ ]:



