#!/bin/bash

EXE="/usr/data/rudzinski/soft_backup2/Gromacs_Tables_07.02.13/gmx4_tables-intracut"

INPUT="f_zero.dat"
OUTPUT="table.xvg"
TYPE="nb"
BASIS="linear"
RMAX="3.00"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX

INPUT="f_forces.IntraMolec_NB_Pair.total.intra_1-4.dat"
OUTPUT="tablep.xvg"
TYPE="intra"
BASIS="Bspline"
RMAX="3.00"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX

INPUT="f_forces.BondStretch.total.bonds.dat"
OUTPUT="table_b0.xvg"
TYPE="bond"
BASIS="linear"
RMIN="0.0"
RMAX="0.800"
SLOPE="200000.0"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMIN $RMAX

INPUT="f_forces.Angle.total.angles.dat"
OUTPUT="table_a0.xvg"
TYPE="angle"
BASIS="linear"
SLOPE="0.5"
$EXE $INPUT $OUTPUT $TYPE $BASIS

INPUT="f_forces.Dihedral.total.dihedrals.dat"
OUTPUT="table_d0.xvg"
TYPE="dihedral"
BASIS="Bspline"
$EXE $INPUT $OUTPUT $TYPE $BASIS

