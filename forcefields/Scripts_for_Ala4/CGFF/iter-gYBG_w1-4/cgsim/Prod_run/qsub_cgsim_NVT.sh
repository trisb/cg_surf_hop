#!/bin/bash
#PBS -S /bin/bash
#PBS -l nodes=4,walltime=2:00:00
#PBS -N mart_polyALA_Equil_PRODRUN
#PBS -V
#PBS -m abe
#PBS -j oe
#PBS -M jfr148@psu.edu
#PBS -q lionxf-noid


#cd $PBS_O_WORKDIR

module load openmpi/gnu

./run_gmx.sh 
