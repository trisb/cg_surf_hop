import numpy as np

d = np.genfromtxt('dihedrals_trace.xvg')[:,1]
r14 = np.genfromtxt('bonds_1-4_trace.xvg')[:,1]

traj = np.vstack((d,r14))

np.savetxt('traj_intra.dat',traj.T)
