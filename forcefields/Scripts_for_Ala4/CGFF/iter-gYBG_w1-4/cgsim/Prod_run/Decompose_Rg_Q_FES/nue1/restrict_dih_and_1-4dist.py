#!/usr/bin/python

from math import sqrt

col0 = []
col1 = []

filename = open("../../dih_trace.xvg","r")
if filename:
   for line in filename:
       line = line.strip()
       col0.append(line.split()[0])
       col1.append(line.split()[1])

yAB = 0.00
yEF = 0.00
yGH = 0.00
i = 0
ctr = 0
keep = -1.00
filename = open("../../distance_1-4.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    yAB = 0.000766*float(col1[i]) + 0.5981
    if ( yAB < float(line.split()[1]) ):
      yEF = 0.00391*float(col1[i]) + 0.926
      if ( yEF > float(line.split()[1]) ):
        yGH = -0.0185*float(col1[i]) + 3.727
        if ( yGH < float(line.split()[1]) ):
          yCD = -0.001985*float(col1[i]) + 1.3879
          if ( yCD > float(line.split()[1]) ):
            keep = 0.00
            ctr += 1
          else:
            keep = 1.00
        else:
          keep = 1.00
      else:
        yCD = -0.001985*float(col1[i]) + 0.673
        if ( yCD > float(line.split()[1]) ):
          keep = 0.00
          ctr += 1
        else:
          keep = 1.00
    else:
      keep = 1.00

#    print str(col0[i]) +" "+str(keep)
    i += 1

print str(ctr)
