#!/usr/bin/python
from math import sqrt
from math import fabs

berr = []
d2berr = 0.00
bAA = []
d2bAA = 0.00

lammin = 0.05
lamold = 1.00
lam = 1.00
kT = 2.4777

N_part=4
part_name = ["nuh", "num", "nue1", "nue2"]
wAA = [ 0.1542, 0.2524, 0.2609, 0.3325 ]
wold = [0 for x in xrange(N_part)]
w = [0 for x in xrange(N_part)]
wtot = 0
lam = 0.25

i = 0
filename = open("inp.txt","r")
if filename:
  for line in filename:
      line = line.strip()
      if ( (i!=0) & (i!=5) ):
        wold[i-1] = line.split()[1]
      i += 1


for i in range(0,N_part):
  filename = open("N_"+str(part_name[i])+".dat","r")
  if filename:
    for line in filename:
        line = line.strip()
        w[i] = line.split()[0]
        wtot += float(w[i])

for i in range(0,N_part):
  w[i] = float(w[i]) / float(wtot)

fp = open("used_weights.dat","w+")
for i in range(0,N_part):
  fp.write(str(wold[i])+'\n')

fp = open("sampled_weights.dat","w+")
for i in range(0,N_part):
  fp.write(str(w[i])+'\n')

#for i in range(0,N_part):
#  w[i] = float(wold[i]) + float(lam)*( float(wold[i]) - float(w[i]) )

fp = open("diff_weights.dat","w+")
for i in range(0,N_part):
  w[i] = float(wold[i]) - float(w[i])
  fp.write(str(w[i])+'\n')

#fp = open("inp_new.txt","w+")
#fp.write("[Struct_Files]   4"+'\n')

#for i in range(0,N_part):
#  fp.write("cg"+str(part_name[i])+".trr  "+str(w[i])+'\n')

#fp.write("[End Struct_Files]"+'\n')


