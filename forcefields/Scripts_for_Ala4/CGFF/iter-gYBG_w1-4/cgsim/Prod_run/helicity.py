#!/usr/bin/python

from math import sqrt
from math import exp

Q = []
var = 25.0
r0 = 0.5
Qi = 0.00
Ni = 9.0

filename = open("distance_bonds_1-4.dat.xvg","r")
if filename:
   for line in filename:
       line = line.strip()
       Qi = exp( -var*(float(line.split()[1])-r0)**2 )
       Q.append(Qi)

i = 0
filename = open("distance_bonds_2-5.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi     
    i += 1

i = 0
filename = open("distance_bonds_3-6.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_4-7.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_5-8.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_6-9.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_7-10.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_8-11.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1

i = 0
filename = open("distance_bonds_9-12.dat.xvg","r")
if filename:
  for line in filename.readlines ():
    line = line.strip()
    Qi = exp( -var*(float(line.split()[1])-r0)**2 )
    Q[i] += Qi
    i += 1


N = len(Q)
for i in range(1,N):
  
  Q[i] /= Ni
  print str(Q[i])

