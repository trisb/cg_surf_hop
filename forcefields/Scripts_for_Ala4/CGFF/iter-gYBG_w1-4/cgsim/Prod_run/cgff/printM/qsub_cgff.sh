#!/bin/bash
#PBS -S /bin/bash
#PBS -l nodes=4,walltime=01:00:00
#PBS -N cgff-polyALA-FM_noH2O
#PBS -V
#PBS -m abe
#PBS -j oe
#PBS -M jfr148@psu.edu
#PBS -q lionxf-noid


cd $PBS_O_WORKDIR

module load openmpi/gnu/1.6.5

module load mkl/11.0.0.079

# mpirun -np 8 -machinefile $PBS_NODEFILE  /home5/jwm289/CGFF/working_version/src/CG_ff-gmx
# mpirun /home5/jwm289/CGFF/mpi_versions/wm_mpi_CGFF_version_6.5.8/CG_ff-gmx
# mpirun /gpfs/home/jfr148/CGFF/bin/CG_ff-gmx-mpi-JFR-current-4.5.3-noDR4-noSVD --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/bin/CG_ff-gmx-mpi-JFR-current-4.5.3-printMtotal-posttrimMb --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/pre_04.06.12/working_version_Joe_current_4.5.3_noDR4_noSVD_usebref/CG_ff-gmx-mpi-JFR-current-4.5.3-noDR4-noSVD-usebref --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/pre_04.06.12/working_version_Joe_current_4.5.3_noDR4_noSVD_calcbref/CG_ff-gmx-mpi-JFR-current-4.5.3-noDR4-noSVD-calcbref --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/pre_04.06.12/working_version_Joe_current_4.5.3_noDR4_noSVD_calcbref_splitfiles/CG_ff-gmx-mpi-JFR-current-4.5.3-noDR4-noSVD-calcbref-splitfiles --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/testing_version_4.5.3_08.02.12/CG_ff-gmx-mpi-JFR-4.5.3-test-08-02-12-FE5e-4 --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/testing_version_4.5.3_09.10.12_dp/CG_ff-gmx-mpi-JFR-4.5.3-test-09-10-12-dp-FE5e-4 --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/testing_version_4.5.3_02.26.13_dp/CG_ff-gmx-mpi-JFR-4.5.3-test-02-26-13-dp --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/testing_version_4.5.3_02.26.13_dp_bond-angleonlyAAM2_nbregonly_printanaldih/CG_ff-gmx-mpi-JFR-4.5.3-test-02-26-13-dp-nbregonly-beta0.01 --mca pml ob1
#mpirun /gpfs/home/jfr148/CGFF/4.5.3/testing_version_4.5.3_02.26.13_dp_bond-angleonlyAAM2_nbregonly_printanaldih_bsolnerror/CG_ff-gmx-mpi-JFR-4.5.3-test-02-26-13-dp-nbregonly-beta0.1 --mca pml ob1
mpirun /gpfs/home/jfr148/CGFF/4.5.3/working_versions/working_version_4.5.3_12.03.13_dp_filedebug/CG_ff-gmx-mpi-JFR-4.5.3-12-03-13-dp --mca pml ob1
