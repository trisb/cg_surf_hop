axes('Units','inches','position', [1 ,1, 3.5, 3.5]);
n = hist3(dat,[221 201]);
n1 = n';
%n1(size(n,1)+1,size(n,2)+1) = 0;
xb = linspace(min(dat(:,1)),max(dat(:,1)),size(n,1));
yb = linspace(min(dat(:,2)),max(dat(:,2)),size(n,2));
%pcolor(xb,yb,n');
%set(h, 'zdata', ones(size(n1)) * -max(max(n))) 
%colormap(hot) % heat map 
%grid on 
%view(3);

prob = n1 / 200001
%pcolor(prob)

FEL = -2.477721 .* log (prob)
%FEL = log (n1)
Final = (FEL - 30.2433)/2.477572
%Final = (FEL - 25.2512)/2.477572
pcolor(xb,yb,Final)

%pcolor(n1)

set(gca,'layer','top')
xlabel('R_g (nm)','fontsize',14,'fontweight','b')
ylabel('RMSD (nm)','fontsize',14,'fontweight','b')
axis([0.4 1.2 0 0.7])
% zoom
%axis([0.45 0.75 0 0.45])
set(gca,'YDir','reverse');
set(gca,'Ytick',[0, 0.35, 0.7], 'fontsize', 10)
set(gca,'Xtick',[0.4, 0.8, 1.2 ], 'fontsize', 10)
% zoom
%set(gca,'Ytick',[0, 0.45], 'fontsize', 10 )
%set(gca,'Xtick',[0.45, 0.75 ], 'fontsize', 10 )
%set(gca,'XtickLabel',{'-3','-1.5','0','1.5','3'})
%set(gca,'YtickLabel',{'-3','-1.5','0','1.5','3'})
set(gca,'ticklength',5*get(gca,'ticklength'))
shading flat
colorbar
set(gca, 'CLim', [-6, 0]);
%set(colorbar,'YTick',[0 -0.2 -0.4 -0.6 -0.8 -1]);
colormap(jet(200))
set(get(colorbar,'YLabel'),'String','k_BT','fontsize',12,'fontweight','b' )
%set(colorbar,'fontsize', 10, 'fontweight', 'b')
axis square

