import numpy as np

traj = np.genfromtxt('cvs_time-4s.dat')
traj = np.array([traj[:,4]*180./np.pi,traj[:,1]])
traj = traj.T
traj = traj.reshape(1,traj.shape[0],traj.shape[1])
np.save('dtraj_CG_1traj_dih_1-4',traj)
