*******************************************************************************************

  Mode: GROMACS
  Temperature: 300.000000


*******************************************************************************************

  N_coeff: 169

*******************************************************************************************

  N_structures: 1
    Structure: 1	 Weight: 1.000000	 Name: ../../../Assign/3states/cgALA4.state2.trr

*******************************************************************************************

  N_Site_Types: 1
    Site_Type: 1	 name: CA

*******************************************************************************************

  N_Inter_Types: 4
    Inter_Type: 1
    Inter_Name: intra_1-4    Inter_Type: IntraMolec_NB_Pair
    Basis: Bspline    i_basis: 6
    dr: 0.020000    R_min: 0.200000    R_max: 1.200000
    N_pts: 51    N_coeff: 51    n_smooth: 0    i_0: 0
    kspline: 4

    Inter_Type: 2
    Inter_Name: bonds    Inter_Type: BondStretch
    Basis: linear    i_basis: 5
    dr: 0.002000    R_min: 0.350000    R_max: 0.410000
    N_pts: 31    N_coeff: 31    n_smooth: 0    i_0: 51

    Inter_Type: 3
    Inter_Name: angles    Inter_Type: Angle
    Basis: linear    i_basis: 5
    dr: 0.034907    R_min: 0.872665    R_max: 2.792527
    N_pts: 56    N_coeff: 56    n_smooth: 0    i_0: 82

    Inter_Type: 4
    Inter_Name: dihedrals    Inter_Type: Dihedral
    Basis: Bspline    i_basis: 6
    dr: 0.209440    R_min: -3.141593    R_max: 3.141593
    N_pts: 31    N_coeff: 31    n_smooth: 0    i_0: 138
    kspline: 4

*******************************************************************************************

  N_Pair_Interactions: 0
*******************************************************************************************

  N_BondStretch: 1 
    BondStretch: 1 
    Name: BondStretch    Inter_Name: bonds    Site Types: CA-CA
    Basis: linear    i_basis: 5    i_0: 51    N_pts: 31    N_coeff: 31 
    dr: 0.002000     R_0: 0.350000     R_max: 0.410000     n_smooth: 0 

*******************************************************************************************

  N_Angle: 1 
    Angle: 1 
    Name: Angle    Inter_Name: angles    Site Types: CA-CA-CA
    Basis: linear    i_basis: 5    i_0: 82    N_pts: 56    N_coeff: 56 
    dr: 0.034907     R_0: 0.872665     R_max: 2.792527     n_smooth: 0 

*******************************************************************************************

  N_Dihedral: 1 
    Dihedral: 1 
    Name: Dihedral    Inter_Name: dihedrals    Site Types: CA-CA-CA-CA
    Basis: Bspline    i_basis: 6    i_0: 138    N_pts: 31    N_coeff: 31 
    dr: 0.209440     R_0: -3.141593     R_max: 3.141593     n_smooth: 0 
   kspline: 4

*******************************************************************************************

  N_IntraMolec_NB_Pair: 1 
    IntraMolec_NB_Pair: 1 
    Name: IntraMolec_NB_Pair    Inter_Name: intra_1-4    Site Types: CA-CA
    Basis: Bspline    i_basis: 6    i_0: 0    N_pts: 51    N_coeff: 51 
    dr: 0.020000     R_0: 0.200000     R_max: 1.200000     n_smooth: 0 
   kspline: 4

*******************************************************************************************

*******************************************************************************************

  printing SVD parameters ...  
    rcond: -1.000000 
    flag_printSV: YES 
    flag_printevecs: NO 
    flag_solve: YES 
*******************************************************************************************

  printing TPR filenames ...  
    N_TPR: 1 
    TPR[0]: cgQuad.tpr 
*******************************************************************************************

  printing Metric_Tensor parameters ...  
    flag_norm: YES 
    flag_Mcnt: YES 
*******************************************************************************************

  printing Preconditioning options ...  
    RPC_type: dimless 
    flag_normb: NO 
    LPC_type: dimless 
    flag_normphi: NO 
*******************************************************************************************

  printing Trim options ...  
    FE: 0.100000 
*******************************************************************************************

  printing Regularization options ...  
    type: UNCERT 
    Nmax: 0 
    tau_alpha: 0.000000 
    tau_beta: 0.050000 
