#!/bin/bash

EXE="/usr/data/rudzinski/soft_backup2/Gromacs_Tables_07.02.13/gmx4_tables-intracut"

INPUT="f_zero.dat"
OUTPUT="table.xvg"
TYPE="nb"
BASIS="linear"
RMAX="3.00"
SLOPE="-1"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX $SLOPE

INPUT="f_edit.intra_1-4.dat"
OUTPUT="tablep.xvg"
TYPE="intra"
BASIS="Bspline"
RMIN="0.0"
RMAX="3.00"
SLOPE="10"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMAX $SLOPE

INPUT="f_edit.intra_1-4.dat"
OUTPUT="table_b1.xvg"
TYPE="bond"
BASIS="Bspline"
RMIN="0.0"
RMAX="3.00"
SLOPE="50"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMIN $RMAX $SLOPE

INPUT="f_forces.BondStretch.total.bonds.dat"
OUTPUT="table_b0.xvg"
TYPE="bond"
BASIS="linear"
RMIN="0.0"
RMAX="0.800"
SLOPE="50000.0"
$EXE $INPUT $OUTPUT $TYPE $BASIS $RMIN $RMAX $SLOPE

INPUT="f_edit.angles.dat"
OUTPUT="table_a0.xvg"
TYPE="angle"
BASIS="linear"
SLOPE="0.25"
$EXE $INPUT $OUTPUT $TYPE $BASIS $SLOPE

INPUT="f_edit.dihedrals.dat"
OUTPUT="table_d0.xvg"
TYPE="dihedral"
BASIS="Bspline"
SLOPE="-1"
$EXE $INPUT $OUTPUT $TYPE $BASIS $SLOPE

