;
; STANDARD MD INPUT OPTIONS FOR MARTINI 2.0 or 2.1
;

; TIMESTEP IN MARTINI 
; Most simulations are numerically stable 
; with dt=40 fs, some (especially rings) require 20-30 fs.
; Note that time steps of 40 fs and larger may create local heating or 
; cooling in your system. Although the use of a heat bath will globally 
; remove this effect, it is advised to check consistency of 
; your results for somewhat smaller time steps in the range 20-30 fs.
; Time steps exceeding 40 fs should not be used; time steps smaller
; than 20 fs are also not required.

integrator               = steep
tinit                    = 0.0
dt                       = 0.002  
nsteps                   = 5000000
nstcomm                  = 1
comm-grps		 = 

nstxout                  = 10000
nstvout                  = 10000
nstfout                  = 10000
nstlog                   = 10000
nstenergy                = 10000
nstxtcout                = 0
energygrps               = Protein 

; NEIGHBOURLIST and MARTINI 
; Due to the use of shifted potentials, the noise generated 
; from particles leaving/entering the neighbour list is not so large, 
; even when large time steps are being used. In practice, once every 
; ten steps works fine with a neighborlist cutoff that is equal to the 
; non-bonded cutoff (1.2 nm). However, to improve energy conservation 
; or to avoid local heating/cooling, you may increase the update frequency 
; and/or enlarge the neighbourlist cut-off (to 1.4 nm). The latter option 
; is computationally less expensive and leads to improved energy conservation

nstlist                  = 1
ns_type                  = grid
pbc                      = xyz
rlist                    = 1.5

; MARTINI and NONBONDED 
; Standard cut-off schemes are used for the non-bonded interactions 
; in the Martini model: LJ interactions are shifted to zero in the 
; range 0.9-1.2 nm, and electrostatic interactions in the range 0.0-1.2 nm. 
; The treatment of the non-bonded cut-offs is considered to be part of 
; the force field parameterization, so we recommend not to touch these 
; values as they will alter the overall balance of the force field.
; In principle you can include long range electrostatics through the use
; of PME, which could be more realistic in certain applications 
; Please realize that electrostatic interactions in the Martini model are 
; not considered to be very accurate to begin with, especially as the 
; screening in the system is set to be uniform across the system with 
; a screening constant of 15. When using PME, please make sure your 
; system properties are still reasonable.

coulombtype              = user 
rcoulomb                 = 1.5
vdw_type                 = user
rvdw                     = 1.5

; MARTINI and TEMPRATURE/PRESSURE
; normal temperature and pressure coupling schemes can be used. 
; It is recommended to couple individual groups in your system separately.
; Good temperature control can be achieved with the Berendsen thermostat, 
; using a coupling constant of the order of Ï„ = 1 ps. Even better 
; temperature control can be achieved by reducing the temperature coupling 
; constant to 0.1 ps, although with such tight coupling (Ï„ approaching 
; the time step) one can no longer speak of a weak-coupling scheme.
; We therefore recommend a coupling time constant of at least 0.5 ps.
;
; Similarly, pressure can be controlled with the Berendsen barostat, 
; with a coupling constant in the range 1-5 ps and typical compressibility 
; in the order of 10-4 - 10-5 bar-1. Note that, in order to estimate 
; compressibilities from CG simulations, you should use Parrinello-Rahman 
; type coupling.

tcoupl                   = Berendsen
tc-grps                  = system
tau_t                    = 1.0 
ref_t                    = 298 
;Pcoupl                   = berendsen 
;Pcoupltype               = isotropic
;tau_p                    = 2.0 
;compressibility          = 3e-4
;ref_p                    = 1.0

gen_vel                  = yes
gen_temp                 = 298
gen_seed                 = 473529

; MARTINI and CONSTRAINTS 
; for ring systems constraints are defined
; which are best handled using Lincs. 
; Note, during energy minimization the constrainst should be
; replaced by stiff bonds.

;constraints              = none 
;constraint_algorithm     = Lincs
;unconstrained_start      = no
;lincs_order              = 4
;lincs_warnangle          = 30

; JFR - Freeze protein for approximate water equilibration
;ENERGY GROUP EXCLUSIONS
;energygrp_excl           = Protein Protein

;NON-EQUILIBRIUM MD
;freezegrps               = Protein 
;freezedim                = Y Y Y

;define = -DPOSRES
