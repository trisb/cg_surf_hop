Nstates=3

for i in `seq 0 $((${Nstates}-1))`;
do
    trjconv -f ../../sim/cgALA4.cat10.renum.trr -s ../../sim/cgALA4.tpr -drop drop_state${i}.xvg -dropunder 0.5 -o cgALA4.state${i}.trr -force <<-EOF
0
EOF

done

