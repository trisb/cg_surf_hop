# CG Surface Hopping

* Tristan Bereau and Joseph F. Rudzinski
* Max Planck Institute for Polymer Research, Mainz, Germany
* 2018


This repository contains all scripts and analysis for *hexane* and
tetraalanine (*ALA4*).  Each directory contains an ipython notebook
containing the analysis of the distributions and free-energy surfaces.
The subdirectories `?state(s)/` contain the scripts to run the CG Surface
Hopping simulations in Espresso++.

The simulations require Espresso++ with the Surface Hopping feature,
which can be found in

[https://github.com/tbereau/espressopp](https://github.com/tbereau/espressopp)

Code tested and run on commit `3a6a943`.

## Ala4 4S time-series

To extract the long time-series for ALA4 4-state model (4S), use `git lfs`:
```
git lfs checkout
```
which will download `ala4/4states_w14/cvs_time.dat`.

## Forcefield-preparation scripts

The subfolder `forcefields` contains all files necessary to decompose the
reference trajectories into clusters and prepare each conformationally-dependent
force field. 

All trajectory files stored in `.trr` format are stored using `git lfs`. Use
```
git lfs checkout
```
which will download and extract all `*.trr` files.

A list of external software used to construct the force fields is provided in
`forcefields/soft/README.md`