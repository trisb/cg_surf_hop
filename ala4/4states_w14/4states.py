#!/usr/bin/env python2

import sys
import time
import os
import mpi4py.MPI as MPI
import espressopp
import math
import numpy as np
import logging
import copy
from espressopp import Real3D, Int3D
from espressopp.tools import timers


# Input values for system
N = 10                                    # box size
size  = (float(N), float(N), float(N))
numParticles = 4                          # number of particles
nsnapshots = 5000000
nsteps  = 10                             # number of steps
skin    = 0.03                             # skin for Verlet lists
cutoff  = 0.2
splinetypes = ['Linear','Akima','Cubic']  # implemented spline types


######################################################################
## IT SHOULD BE UNNECESSARY TO MAKE MODIFICATIONS BELOW THIS LINE   ##
######################################################################

# print '\n-- Tabulated Interpolation Test -- \n'
# print 'Steps: %3s' % nsteps
# print 'Particles: %3s' % numParticles
# print 'Cutoff: %3s' % cutoff

dim = 1
spline = 2

# compute the number of cells on each node
def calcNumberCells(size, nodes, cutoff):
    ncells = 1
    while size / (ncells * nodes) >= cutoff:
       ncells = ncells + 1
    return ncells - 1


# set up system
system = espressopp.System()
system.rng  = espressopp.esutil.RNG()
system.bc = espressopp.bc.OrthorhombicBC(system.rng, size)
system.skin = skin

comm = MPI.COMM_WORLD

nodeGrid = Int3D(1, 1, comm.size)
cellGrid = Int3D(
    calcNumberCells(size[0], nodeGrid[0], cutoff),
    calcNumberCells(size[1], nodeGrid[1], cutoff),
    calcNumberCells(size[2], nodeGrid[2], cutoff)
    )
system.storage = espressopp.storage.DomainDecomposition(system, nodeGrid, cellGrid)

pid = 0

system.storage.addParticle(0, Real3D(0.15, 0.1, 0))
system.storage.addParticle(1, Real3D(0.4, 0.1, 0))
system.storage.addParticle(2, Real3D(0.6, 0, 0))
system.storage.addParticle(3, Real3D(0.7, 0.1, 0))

system.storage.decompose()

# integrator
integrator = espressopp.integrator.VelocityVerlet(system)
integrator.dt = 0.001

# now build Verlet List
# ATTENTION: you must not add the skin explicitly here
logging.getLogger("Interpolation").setLevel(logging.INFO)
vl = espressopp.VerletList(system, cutoff = cutoff)

alpha      = 0.05

# bonds
fpl = espressopp.FixedPairList(system.storage)
fpl.addBonds([(0,1),(1,2),(2,3)])
potBond = espressopp.interaction.Tabulated(itype=spline, filename="table_b0.xvg")
interBond = espressopp.interaction.FixedPairListTabulated(system, fpl, potBond)
system.addInteraction(interBond)

# mu and sd per state for angle, dihedral, bond_1-4
mu_0 = [1.62098, 0.889793, 0.552226]
sd_0 = [0.0803862, 0.380702, 0.051172]
mu_1 = [2.15395, -2.07566, 0.960086]
sd_1 = [0.106535, 0.575717, 0.060221]
mu_2 = [1.87219, -2.69338, 0.907892]
sd_2 = [0.0919977, 0.483539, 0.045374]
sd   = [12.219838*math.pi/180., 108.612955*math.pi/180., 0.157725]
pops = [0.167432651347, 0.35025099498, 0.22379752405, 0.258518829623]

# angles couple to r14 and dihedral
filenames = ["table_a0_rad.xvg", "table_a1_rad.xvg", "table_a2_rad.xvg", "table_a3_rad.xvg"]
angles = [(0,1,2),(1,2,3)]
partner = [(1,2,3),(0,1,2)]
potAngle1 = espressopp.interaction.TabulatedSubEnsAngular()
potAngle2 = espressopp.interaction.TabulatedSubEnsAngular()
potAngles = [potAngle1, potAngle2]
interAngles = []
for a,p,pa in zip(angles,partner,potAngles):
    fpl = espressopp.FixedTripleList(system.storage)
    fpl.addTriples([a])
    pa.addInteraction(1, filenames[0],
        espressopp.RealND([mu_0[0], mu_0[2], mu_0[1], sd_0[0], sd_0[2], sd_0[1]]))
    pa.addInteraction(1, filenames[1],
        espressopp.RealND([mu_1[0], mu_1[2], mu_1[1], sd_1[0], sd_1[2], sd_1[1]]))
    pa.addInteraction(1, filenames[2],
        espressopp.RealND([mu_2[0], mu_2[2], mu_2[1], sd_2[0], sd_2[2], sd_2[1]]))
    pa.addInteraction(1, filenames[3],
        espressopp.RealND([0., 0., 0., 0., 0., 0.]))
    pa.alpha_set(alpha)
    # Renormalize the CVs
    pa.colVarSd_set(0,  sd[0])
    pa.colVarSd_set(1,  sd[2])
    pa.colVarSd_set(2,  sd[1])
    pa.targetProb_set(0, pops[0])
    pa.targetProb_set(1, pops[1])
    pa.targetProb_set(2, pops[2])
    pa.targetProb_set(3, pops[3])
    # Partners
    cv_bl = espressopp.FixedPairList(system.storage)
    cv_bl.addBonds([(0,3)])
    pa.colVarBondList = cv_bl
    cv_al1 = espressopp.FixedTripleList(system.storage)
    cv_al1.addTriples([p])
    pa.colVarAngleList = cv_al1
    cv_dl = espressopp.FixedQuadrupleList(system.storage)
    cv_dl.addQuadruples([(0,1,2,3)])
    pa.colVarDihedList = cv_dl
    interAngle = espressopp.interaction.FixedTripleListTabulatedSubEnsAngular(system, fpl, pa)
    interAngles.append(interAngle)
    system.addInteraction(interAngle)

# r_{14} couples to the dihedral
filenames = ["table_r0.xvg", "table_r1.xvg", "table_r2.xvg", "table_r3.xvg"]
fpl = espressopp.FixedPairList(system.storage)
fpl.addBonds([(0,3)])
potBond14 = espressopp.interaction.TabulatedSubEns()
potBond14.addInteraction(1, filenames[0],
        espressopp.RealND([mu_0[2], mu_0[0], mu_0[1], sd_0[2], sd_0[0], sd_0[1]]))
potBond14.addInteraction(1, filenames[1],
        espressopp.RealND([mu_1[2], mu_1[0], mu_1[1], sd_1[2], sd_1[0], sd_1[1]]))
potBond14.addInteraction(1, filenames[2],
        espressopp.RealND([mu_2[2], mu_2[0], mu_2[1], sd_2[2], sd_2[0], sd_2[1]]))
potBond14.addInteraction(1, filenames[3],
        espressopp.RealND([0., 0., 0., 0., 0., 0.]))
potBond14.alpha_set(alpha)
# Renormalize the CVs
potBond14.colVarSd_set(0,  sd[2]) # r14
potBond14.colVarSd_set(1,  sd[0]) # angle
potBond14.colVarSd_set(2,  sd[1]) # dihedral
potBond14.targetProb_set(0, pops[0])
potBond14.targetProb_set(1, pops[1])
potBond14.targetProb_set(2, pops[2])
potBond14.targetProb_set(3, pops[3])
# Partners
cv_al = espressopp.FixedTripleList(system.storage)
cv_al.addTriples([(0,1,2),(1,2,3)])
potBond14.colVarAngleList = cv_al
cv_dl = espressopp.FixedQuadrupleList(system.storage)
cv_dl.addQuadruples([(0,1,2,3)])
potBond14.colVarDihedList = cv_dl
interBond14 = espressopp.interaction.FixedPairListTabulatedSubEns(system, fpl, potBond14)
system.addInteraction(interBond14)

# dihedral couples to r_{14}
ftl = espressopp.FixedQuadrupleList(system.storage)
ftl.addQuadruples([(0,1,2,3)])
filenames = ["table_d0_rad.xvg", "table_d1_rad.xvg", "table_d2_rad.xvg", "table_d3_rad.xvg"]
potDihed = espressopp.interaction.TabulatedSubEnsDihedral()
potDihed.alpha_set(alpha)
# RealND: dihed, bond, angle, sd_dihed, sd_bond, sd_angle
potDihed.addInteraction(1, filenames[0],
    espressopp.RealND([mu_0[1], mu_0[2], mu_0[0], sd_0[1], sd_0[2], sd_0[0]]))
potDihed.addInteraction(1, filenames[1],
    espressopp.RealND([mu_1[1], mu_1[2], mu_1[0], sd_1[1], sd_1[2], sd_1[0]]))
potDihed.addInteraction(1, filenames[2],
    espressopp.RealND([mu_2[1], mu_2[2], mu_2[0], sd_2[1], sd_2[2], sd_2[0]]))
potDihed.addInteraction(1, filenames[3],
    espressopp.RealND([0., 0., 0., 0., 0., 0.]))
cv_bl = espressopp.FixedPairList(system.storage)
cv_bl.addBonds([(0,3)])
potDihed.colVarBondList = cv_bl
cv_al = espressopp.FixedTripleList(system.storage)
cv_al.addTriples([(0,1,2),(1,2,3)])
potDihed.colVarAngleList = cv_al
# Renormalize the CVs
potDihed.colVarSd_set(0, sd[1]) # dihedral
potDihed.colVarSd_set(1, sd[2])
potDihed.colVarSd_set(2, sd[0])
potDihed.targetProb_set(0, pops[0])
potDihed.targetProb_set(1, pops[1])
potDihed.targetProb_set(2, pops[2])
potDihed.targetProb_set(3, pops[3])
interDihed = espressopp.interaction.FixedQuadrupleListTabulatedSubEnsDihedral(system, ftl, potDihed)
system.addInteraction(interDihed)

print "Finished adding interactions"

temp = espressopp.analysis.Temperature(system)

temperature = temp.compute()
Ek = 0.5 * temperature * (4 * numParticles)
Ep = interAngles[0].computeEnergy() + interAngles[1].computeEnergy() + interDihed.computeEnergy()

# langevin thermostat
langevin = espressopp.integrator.LangevinThermostat(system)
integrator.addExtension(langevin)
langevin.gamma = 10.0
langevin.temperature = 2.479 # in kJ/mol

# sock = espressopp.tools.vmd.connect(system)

configurations = espressopp.analysis.Configurations(system)

weightsr = []
weightsa = []
weightsd = []
ene = []
time_trace = []
start_time = time.clock()
for k in range(nsnapshots):
    integrator.run(nsteps)
    # espressopp.tools.vmd.imd_positions(system, sock)
    temperature = temp.compute()
    Ek = 0.5 * temperature * (4 * numParticles)
    Er, Ea, Ed = interBond14.computeEnergy(), \
        interAngles[0].computeEnergy() + interAngles[1].computeEnergy(), \
        interDihed.computeEnergy()
    wtsr = potBond14.weight_get()
    wtsa1 = potAngle1.weight_get()
    wtsd = potDihed.weight_get()
    cv = potDihed.colVar
    # cv = potAngle1.colVar
    weightsr.append([wtsr[0], wtsr[1], wtsr[2]])
    weightsa.append([wtsa1[0], wtsa1[1], wtsa1[2]])
    weightsd.append([wtsd[0], wtsd[1], wtsd[2]])
    if k % 100 == 0:
        print 'Step %6d:' % ((k+100)*nsteps),
        # print "Weights: (%3f, %3f)" % (wts[0], wts[1]),
        print "W_avg(r): (%3f, %3f, %3f)" % (np.mean([w[0] for w in weightsr]),
                    np.mean([w[1] for w in weightsr]),
                    np.mean([w[2] for w in weightsr])),
        print "W_avg(a): (%3f, %3f, %3f)" % (np.mean([w[0] for w in weightsa]),
                    np.mean([w[1] for w in weightsa]),
                    np.mean([w[2] for w in weightsa])),
        print "W_avg(d): (%3f, %3f, %3f)" % (np.mean([w[0] for w in weightsd]),
                    np.mean([w[1] for w in weightsd]),
                    np.mean([w[2] for w in weightsd])),
        print "ColVar: (%3f, %3f, %3f, %3f)" % (cv[0], cv[1], cv[2], cv[3])
    ene.append([Er, Ea, Ed])
    configurations.gather()
    time_trace.append((k+1)*nsteps)

weightsr = np.array(weightsr)
weightsd = np.array(weightsd)
np.savetxt('weights_time.dat',weightsd,fmt='%f',header="weightsd")
time_trace = np.array(time_trace)

cvs = []
cvs1, cvs2 = [], []
for i,conf in enumerate(configurations):
    b1 = system.bc.getMinimumImageVector(conf[1],conf[0])
    b2 = system.bc.getMinimumImageVector(conf[1],conf[2])
    a1 = math.acos(b1*b2/(b1.abs()*b2.abs()))
    b2 = system.bc.getMinimumImageVector(conf[2],conf[1])
    b3 = system.bc.getMinimumImageVector(conf[2],conf[3])
    a2 = math.acos(b2*b3/(b2.abs()*b3.abs()))
    b4 = system.bc.getMinimumImageVector(conf[0],conf[3])

    r21 = system.bc.getMinimumImageVector(conf[1],conf[0])
    r32 = system.bc.getMinimumImageVector(conf[2],conf[1])
    r43 = system.bc.getMinimumImageVector(conf[3],conf[2])

    rijjk = r21.cross(r32)
    rjkkn = r32.cross(r43)

    rijjk_sqr = rijjk.sqr()
    rjkkn_sqr = rjkkn.sqr()

    rijjk_abs = rijjk.abs()
    rjkkn_abs = rjkkn.abs()

    inv_rijjk = 1.0 / rijjk_abs
    inv_rjkkn = 1.0 / rjkkn_abs

    cos_phi = (rijjk * rjkkn) * (inv_rijjk * inv_rjkkn)
    if (cos_phi > 1.0): cos_phi = 1.0
    elif (cos_phi < -1.0): cos_phi = -1.0

    phi = np.arccos(cos_phi)
    rcross = rijjk.cross(rjkkn)
    signcheck = rcross * r32
    if (signcheck < 0.0): phi *= -1.0

    cvs.append([time_trace[i], b4.abs(), a1, a2, phi, weightsd[i][0], weightsd[i][1], ene[i][0], ene[i][1], ene[i][2]])

cvs = np.array(cvs)
np.savetxt('cvs_time.dat',cvs,fmt="%f",header="t      r14        a0       a1       dih       w0       w1      Er     Ea        Ed")
histo_r, bin_edges = np.histogram(cvs.T[1], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_r14.dat',np.array([bin_edges,histo_r]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[2], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")
histo_a1, bin_edges = np.histogram(cvs.T[3], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a1.dat',np.array([bin_edges,histo_a1]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[4], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_d0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")

end_time = time.clock()
# timers.show(integrator.getTimers(), precision=2)

# print '\nDone.'
