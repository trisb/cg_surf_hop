#!/usr/bin/env python2

import sys
import time
import os
import mpi4py.MPI as MPI
import espressopp
import math
import numpy as np
import logging
import copy
from espressopp import Real3D, Int3D
from espressopp.tools import timers


# Input values for system
N = 10                                    # box size
size  = (float(N), float(N), float(N))
numParticles = 4                          # number of particles
nsnapshots = 1000000
nsteps  = 10                             # number of steps
skin    = 0.03                             # skin for Verlet lists
cutoff  = 0.2
splinetypes = ['Linear','Akima','Cubic']  # implemented spline types


######################################################################
## IT SHOULD BE UNNECESSARY TO MAKE MODIFICATIONS BELOW THIS LINE   ##
######################################################################

# print '\n-- Tabulated Interpolation Test -- \n'
# print 'Steps: %3s' % nsteps
# print 'Particles: %3s' % numParticles
# print 'Cutoff: %3s' % cutoff

dim = 1
spline = 2

# compute the number of cells on each node
def calcNumberCells(size, nodes, cutoff):
    ncells = 1
    while size / (ncells * nodes) >= cutoff:
       ncells = ncells + 1
    return ncells - 1


# set up system
system = espressopp.System()
system.rng  = espressopp.esutil.RNG()
system.bc = espressopp.bc.OrthorhombicBC(system.rng, size)
system.skin = skin

comm = MPI.COMM_WORLD

nodeGrid = Int3D(1, 1, comm.size)
cellGrid = Int3D(
    calcNumberCells(size[0], nodeGrid[0], cutoff),
    calcNumberCells(size[1], nodeGrid[1], cutoff),
    calcNumberCells(size[2], nodeGrid[2], cutoff)
    )
system.storage = espressopp.storage.DomainDecomposition(system, nodeGrid, cellGrid)

pid = 0

system.storage.addParticle(0, Real3D(6.185298627143168, 5.532360067396171, 2.881803420228981))
system.storage.addParticle(1, Real3D(5.8840591735876915, 5.387053694812786, 2.6984406328983472))
system.storage.addParticle(2, Real3D(5.50010903192068, 5.432852566773995, 2.6741705560742517))
system.storage.addParticle(3, Real3D(5.189652275400706, 5.4484106668019985, 2.891147490672169))

system.storage.decompose()

# integrator
integrator = espressopp.integrator.VelocityVerlet(system)
integrator.dt = 0.001

# now build Verlet List
# ATTENTION: you must not add the skin explicitly here
logging.getLogger("Interpolation").setLevel(logging.INFO)
vl = espressopp.VerletList(system, cutoff = cutoff)

scaling_cv = 0.4
alpha      = 0.05

# bonds
fpl = espressopp.FixedPairList(system.storage)
fpl.addBonds([(0,1),(1,2),(2,3)])
potBond = espressopp.interaction.Tabulated(itype=spline, filename="table_b0.xvg")
interBond = espressopp.interaction.FixedPairListTabulated(system, fpl, potBond)
system.addInteraction(interBond)

# 1-4 bond
fpl14 = espressopp.FixedPairList(system.storage)
fpl14.addBonds([(0,3)])
potBond14 = espressopp.interaction.Tabulated(itype=spline, filename="tablep_esp.xvg")
interBond14 = espressopp.interaction.FixedPairListTabulated(system, fpl14, potBond14)
system.addInteraction(interBond14)

# angles
ftl = espressopp.FixedTripleList(system.storage)
ftl.addTriples([(0,1,2),(1,2,3)])
potAngle = espressopp.interaction.TabulatedAngular(itype=spline, filename="table_a0_rad.xvg")
interAngle = espressopp.interaction.FixedTripleListTabulatedAngular(system, ftl, potAngle)
system.addInteraction(interAngle)

# dihedral
fql = espressopp.FixedQuadrupleList(system.storage)
fql.addQuadruples([(0,1,2,3)])
potDihed = espressopp.interaction.TabulatedDihedral(itype=spline, filename="table_d0_rad.xvg")
interDihed = espressopp.interaction.FixedQuadrupleListTabulatedDihedral(system, fql, potDihed)
system.addInteraction(interDihed)

temp = espressopp.analysis.Temperature(system)

temperature = temp.compute()
Ek = 0.5 * temperature * (4 * numParticles)
Ep = interAngle.computeEnergy() + interDihed.computeEnergy()

# langevin thermostat
langevin = espressopp.integrator.LangevinThermostat(system)
integrator.addExtension(langevin)
langevin.gamma = 10.0
langevin.temperature = 2.479 # in kJ/mol

# sock = espressopp.tools.vmd.connect(system)

configurations = espressopp.analysis.Configurations(system)

ene = []
time_trace = []
start_time = time.clock()
for k in range(nsnapshots):
    integrator.run(nsteps)
    # espressopp.tools.vmd.imd_positions(system, sock)
    temperature = temp.compute()
    Ek = 0.5 * temperature * (4 * numParticles)
    Eb, Eb14, Ea, Ed = interBond.computeEnergy(), interBond14.computeEnergy(), interAngle.computeEnergy(), interDihed.computeEnergy()
    if k % 100 == 0:
        print 'Step %6d:' % ((k+100)*nsteps),
        print "Ene: (%3f, %3f, %3f, %3f)" % (Eb, Eb14, Ea, Ed)
    ene.append([Eb14, Ea, Ed])
    configurations.gather()
    time_trace.append((k+1)*nsteps)

time_trace = np.array(time_trace)

cvs = []
cvs1, cvs2 = [], []
for i,conf in enumerate(configurations):
    b1 = system.bc.getMinimumImageVector(conf[1],conf[0])
    b2 = system.bc.getMinimumImageVector(conf[1],conf[2])
    a1 = math.acos(b1*b2/(b1.abs()*b2.abs()))
    b2 = system.bc.getMinimumImageVector(conf[2],conf[1])
    b3 = system.bc.getMinimumImageVector(conf[2],conf[3])
    a2 = math.acos(b2*b3/(b2.abs()*b3.abs()))
    b4 = system.bc.getMinimumImageVector(conf[0],conf[3])

    r21 = system.bc.getMinimumImageVector(conf[1],conf[0])
    r32 = system.bc.getMinimumImageVector(conf[2],conf[1])
    r43 = system.bc.getMinimumImageVector(conf[3],conf[2])

    rijjk = r21.cross(r32)
    rjkkn = r32.cross(r43)

    rijjk_sqr = rijjk.sqr()
    rjkkn_sqr = rjkkn.sqr()

    rijjk_abs = rijjk.abs()
    rjkkn_abs = rjkkn.abs()

    inv_rijjk = 1.0 / rijjk_abs
    inv_rjkkn = 1.0 / rjkkn_abs

    cos_phi = (rijjk * rjkkn) * (inv_rijjk * inv_rjkkn)
    if (cos_phi > 1.0): cos_phi = 1.0
    elif (cos_phi < -1.0): cos_phi = -1.0

    phi = np.arccos(cos_phi)
    rcross = rijjk.cross(rjkkn)
    signcheck = rcross * r32
    if (signcheck < 0.0): phi *= -1.0

    cvs.append([time_trace[i], b4.abs(), a1, a2, phi, ene[i][0], ene[i][1], ene[i][2]])

cvs = np.array(cvs)
np.savetxt('cvs_time.dat',cvs,fmt="%f",header="t      r14        a0       a1       dih       w0       w1      Ea        Ed")
histo_r, bin_edges = np.histogram(cvs.T[1], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_r14.dat',np.array([bin_edges,histo_r]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[2], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")
histo_a1, bin_edges = np.histogram(cvs.T[3], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a1.dat',np.array([bin_edges,histo_a1]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[4], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_d0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")

end_time = time.clock()
# timers.show(integrator.getTimers(), precision=2)

# print '\nDone.'
