#!/usr/bin/env python2

import sys
import time
import os
import espressopp
import mpi4py.MPI as MPI
import math
import numpy as np
import logging
from espressopp import Real3D, Int3D
from espressopp.tools import timers


# Input values for system
N = 10                                    # box size
size  = (float(N), float(N), float(N))
numParticles = 3                          # number of particles
nsnapshots = 500000
nsteps  = 10                             # number of steps
skin    = 0.03                             # skin for Verlet lists
cutoff  = 0.2
splinetypes = ['Linear','Akima','Cubic']  # implemented spline types


######################################################################
## IT SHOULD BE UNNECESSARY TO MAKE MODIFICATIONS BELOW THIS LINE   ##
######################################################################

print '\n-- Tabulated Interpolation Test -- \n'
print 'Steps: %3s' % nsteps
print 'Particles: %3s' % numParticles
print 'Cutoff: %3s' % cutoff

dim = 1
spline = 2

# compute the number of cells on each node
def calcNumberCells(size, nodes, cutoff):
    ncells = 1
    while size / (ncells * nodes) >= cutoff:
       ncells = ncells + 1
    return ncells - 1


# set up system
system = espressopp.System()
system.rng  = espressopp.esutil.RNG()
system.bc = espressopp.bc.OrthorhombicBC(system.rng, size)
system.skin = skin

comm = MPI.COMM_WORLD

nodeGrid = Int3D(1, 1, comm.size)
cellGrid = Int3D(
    calcNumberCells(size[0], nodeGrid[0], cutoff),
    calcNumberCells(size[1], nodeGrid[1], cutoff),
    calcNumberCells(size[2], nodeGrid[2], cutoff)
    )
system.storage = espressopp.storage.DomainDecomposition(system, nodeGrid, cellGrid)

pid = 0

system.storage.addParticle(0, Real3D(0.15, 0.1, 0))
system.storage.addParticle(1, Real3D(0.4, 0.1, 0))
system.storage.addParticle(2, Real3D(0.6, 0, 0))


system.storage.decompose()

# integrator
integrator = espressopp.integrator.VelocityVerlet(system)
integrator.dt = 0.001

# now build Verlet List
# ATTENTION: you must not add the skin explicitly here
logging.getLogger("Interpolation").setLevel(logging.INFO)
vl = espressopp.VerletList(system, cutoff = cutoff)


# ATTENTION: auto shift was enabled
# bonds
fpl = espressopp.FixedPairList(system.storage)
fpl.addBonds([(0,1),(1,2)])
potBond = espressopp.interaction.Tabulated(itype=spline, filename="table_b0.xvg")
interBond = espressopp.interaction.FixedPairListTabulated(system, fpl, potBond)
system.addInteraction(interBond)

# angle
ftl = espressopp.FixedTripleList(system.storage)
ftl.addTriples([(0,1,2)])
filenames = ["table_a0_rad.xvg"]
potAngle = espressopp.interaction.TabulatedAngular(itype=spline, filename="table_a0_rad.xvg")
interAngle = espressopp.interaction.FixedTripleListTabulatedAngular(system, ftl, potAngle)
system.addInteraction(interAngle)

temp = espressopp.analysis.Temperature(system)

temperature = temp.compute()
Ek = 0.5 * temperature * (3 * numParticles)
Ep = interBond.computeEnergy() + interAngle.computeEnergy()


print 'Start %5s: tot energy = %10.3f pot = %10.3f kin = %10.3f temp = %10.3f' \
    % ("", Ek + Ep, Ep, Ek, temperature)

# langevin thermostat
langevin = espressopp.integrator.LangevinThermostat(system)
integrator.addExtension(langevin)
langevin.gamma = 10.0
langevin.temperature = 2.479 # in kJ/mol

# sock = espressopp.tools.vmd.connect(system)

configurations = espressopp.analysis.Configurations(system)

start_time = time.clock()
time_trace = []
for k in range(nsnapshots):
    integrator.run(nsteps)
    # espressopp.tools.vmd.imd_positions(system, sock)
    temperature = temp.compute()
    Ek = 0.5 * temperature * (3 * numParticles)
    Ep = interBond.computeEnergy() + interAngle.computeEnergy()
    if k % 100 == 0:
        print 'Step %6d: tot energy = %10.3f pot = %10.3f kin = %10.3f temp = %10.3f' % \
            ((k+1)*nsteps, Ek + Ep, Ep, Ek, temperature)
    configurations.gather()
    time_trace.append((k+1)*nsteps)

cvs = []
for i,conf in enumerate(configurations):
    b1 = system.bc.getMinimumImageVector(conf[1],conf[0])
    b2 = system.bc.getMinimumImageVector(conf[1],conf[2])
    ca = math.acos(b1*b2/(b1.abs()*b2.abs()))
    cvs.append([time_trace[i], b1.abs(), b2.abs(), ca])

cvs = np.array(cvs)
np.savetxt('cvs_time.dat',cvs,fmt="%f",header="t     b0     b1       a0")
histo_b0, bin_edges = np.histogram(cvs.T[1], bins='auto', density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_b0.dat',np.array([bin_edges,histo_b0]).T,fmt="%f")
histo_b1, bin_edges = np.histogram(cvs.T[2], bins='auto', density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_b1.dat',np.array([bin_edges,histo_b1]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[3], bins='auto', density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")

end_time = time.clock()
timers.show(integrator.getTimers(), precision=2)

print '\nDone.'
