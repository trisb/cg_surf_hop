#!/usr/bin/env python2

import sys
import time
import os
import mpi4py.MPI as MPI
import espressopp
import math
import numpy as np
import logging
from espressopp import Real3D, Int3D
from espressopp.tools import timers


# Input values for system
N = 10                                    # box size
size  = (float(N), float(N), float(N))
numParticles = 3                          # number of particles
nsnapshots = 500000
nsteps  = 10                             # number of steps
skin    = 0.03                             # skin for Verlet lists
cutoff  = 0.2
splinetypes = ['Linear','Akima','Cubic']  # implemented spline types


######################################################################
## IT SHOULD BE UNNECESSARY TO MAKE MODIFICATIONS BELOW THIS LINE   ##
######################################################################

# print '\n-- Tabulated Interpolation Test -- \n'
# print 'Steps: %3s' % nsteps
# print 'Particles: %3s' % numParticles
# print 'Cutoff: %3s' % cutoff

dim = 1
spline = 2

# compute the number of cells on each node
def calcNumberCells(size, nodes, cutoff):
    ncells = 1
    while size / (ncells * nodes) >= cutoff:
       ncells = ncells + 1
    return ncells - 1


# set up system
system = espressopp.System()
system.rng  = espressopp.esutil.RNG()
system.bc = espressopp.bc.OrthorhombicBC(system.rng, size)
system.skin = skin

comm = MPI.COMM_WORLD

nodeGrid = Int3D(1, 1, comm.size)
cellGrid = Int3D(
    calcNumberCells(size[0], nodeGrid[0], cutoff),
    calcNumberCells(size[1], nodeGrid[1], cutoff),
    calcNumberCells(size[2], nodeGrid[2], cutoff)
    )
system.storage = espressopp.storage.DomainDecomposition(system, nodeGrid, cellGrid)

pid = 0

system.storage.addParticle(0, Real3D(0.15, 0.1, 0))
system.storage.addParticle(1, Real3D(0.4, 0.1, 0))
system.storage.addParticle(2, Real3D(0.6, 0, 0))

system.storage.decompose()

# integrator
integrator = espressopp.integrator.VelocityVerlet(system)
# Need at most 0.0002 time step to integrate accurately; or friction 10
integrator.dt = 0.001

# now build Verlet List
# ATTENTION: you must not add the skin explicitly here
logging.getLogger("Interpolation").setLevel(logging.INFO)
vl = espressopp.VerletList(system, cutoff = cutoff)

scaling_cv = 0.4
alpha      = 0.05

filenames = ["table_b0.xvg", "table_b1.xvg", "table_b2.xvg"]
# Loop over each bond
bonds = [(0,1),(1,2)]
partner = [(1,2),(0,1)]
potBond1 = espressopp.interaction.TabulatedSubEns()
potBond2 = espressopp.interaction.TabulatedSubEns()
potBonds = [potBond1, potBond2]
for b,p,pb in zip(bonds,partner,potBonds):
    pb.addInteraction(1, filenames[0],
        espressopp.RealND([0.26, 2.99, 0., scaling_cv*0.424, scaling_cv*0.277, 0.]))
    pb.addInteraction(1, filenames[1],
        espressopp.RealND([0.26, 2.20, 0., scaling_cv*0.424, scaling_cv*0.277, 0.]))
    pb.addInteraction(1, filenames[2],
        espressopp.RealND([0., 0., 0., 0., 0., 0.]))
    pb.alpha_set(alpha)
    # Renormalize the CVs
    pb.colVarSd_set(0,   0.0119)
    pb.colVarSd_set(1,  19.081*math.pi/180.) # 0.3
    pb.targetProb_set(0, 0.45)
    pb.targetProb_set(1, 0.14)
    # Partners
    fpl = espressopp.FixedPairList(system.storage)
    fpl.addBonds([b])
    cv_bl1 = espressopp.FixedPairList(system.storage)
    cv_bl1.addBonds([p])
    pb.colVarBondList = cv_bl1
    cv_al = espressopp.FixedTripleList(system.storage)
    cv_al.addTriples([(0,1,2)])
    pb.colVarAngleList = cv_al
    interBond = espressopp.interaction.FixedPairListTabulatedSubEns(system, fpl, pb)
    system.addInteraction(interBond)


# angle
ftl = espressopp.FixedTripleList(system.storage)
ftl.addTriples([(0,1,2)])
filenames = ["table_a0_rad.xvg", "table_a1_rad.xvg", "table_a2_rad.xvg"]
potAngle = espressopp.interaction.TabulatedSubEnsAngular()
# RealND: angle, bond, dihed, sd_angle, sd_bond, sd_dihed
potAngle.addInteraction(1, filenames[0],
    espressopp.RealND([2.99, 0.26, 0., scaling_cv*0.277, scaling_cv*0.424, 0.]))
potAngle.addInteraction(1, filenames[1],
    espressopp.RealND([2.20, 0.26, 0., scaling_cv*0.277, scaling_cv*0.424, 0.]))
potAngle.addInteraction(1, filenames[2],
    espressopp.RealND([0., 0., 0., 0., 0., 0.]))
potAngle.alpha_set(alpha)
cv_bl = espressopp.FixedPairList(system.storage)
cv_bl.addBonds([(0,1),(1,2)])
potAngle.colVarBondList = cv_bl
# print potAngle.colVarBondList.getBonds()

# Renormalize the CVs
potAngle.colVarSd_set(0,  19.081*math.pi/180.) # 0.3
potAngle.colVarSd_set(1,   0.0119)
potAngle.targetProb_set(0, 0.45)
potAngle.targetProb_set(1, 0.14)

interAngle = espressopp.interaction.FixedTripleListTabulatedSubEnsAngular(system, ftl, potAngle)
system.addInteraction(interAngle)

temp = espressopp.analysis.Temperature(system)

temperature = temp.compute()
Ek = 0.5 * temperature * (3 * numParticles)
Ep = interBond.computeEnergy() + interAngle.computeEnergy()


# langevin thermostat
langevin = espressopp.integrator.LangevinThermostat(system)
integrator.addExtension(langevin)
langevin.gamma = 10.0
langevin.temperature = 2.479 # in kJ/mol

# sock = espressopp.tools.vmd.connect(system)

configurations = espressopp.analysis.Configurations(system)

weights = []
weightsb1 = []
weightsb2 = []
ene = []
time_trace = []
start_time = time.clock()
for k in range(nsnapshots):
    integrator.run(nsteps)
    # espressopp.tools.vmd.imd_positions(system, sock)
    temperature = temp.compute()
    Ek = 0.5 * temperature * (3 * numParticles)
    # Eb, Eb2, Ea = interBond.computeEnergy(), interBond2.computeEnergy(), interAngle.computeEnergy()
    Eb, Ea = interBond.computeEnergy(), interAngle.computeEnergy()
    wts = potAngle.weight_get()
    wtsb1 = potBond1.weight_get()
    wtsb2 = potBond2.weight_get()
    cv = potAngle.colVar
    weights.append([wts[0], wts[1]])
    weightsb1.append([wtsb1[0], wtsb1[1]])
    weightsb2.append([wtsb2[0], wtsb2[1]])
    if k % 100 == 0:
        print 'Step %6d:' % ((k+100)*nsteps),
        print "Weights: (%3f, %3f)" % (wts[0], wts[1]),
        print "avg: (%3f, %3f)" % (np.mean([w[0] for w in weights]),
                                    np.mean([w[1] for w in weights])),
        print "ColVar: (%3f, %3f, %3f)" % (cv[0], cv[1], cv[2])
        # print "Ene: (%3f, %3f)" % (Eb, Ea)
    ene.append([Eb, Ea])
    configurations.gather()
    time_trace.append((k+1)*nsteps)

weights = np.array(weights)
np.savetxt('weights_time.dat',weights,fmt='%f',header="weights")
time_trace = np.array(time_trace)

cvs = []
cvs1, cvs2 = [], []
for i,conf in enumerate(configurations):
    b1 = system.bc.getMinimumImageVector(conf[1],conf[0])
    b2 = system.bc.getMinimumImageVector(conf[1],conf[2])
    ca = math.acos(b1*b2/(b1.abs()*b2.abs()))
    cvs.append([time_trace[i], b1.abs(), b2.abs(), ca, weights[i][0], weights[i][1], ene[i][0], ene[i][1]])

cvs = np.array(cvs)
np.savetxt('cvs_time.dat',cvs,fmt="%f",header="t      b0       b1       a0       w0       w1      Eb        Ea")
histo_b0, bin_edges = np.histogram(cvs.T[1], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_b0.dat',np.array([bin_edges,histo_b0]).T,fmt="%f")
histo_b1, bin_edges = np.histogram(cvs.T[2], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_b1.dat',np.array([bin_edges,histo_b1]).T,fmt="%f")
histo_a0, bin_edges = np.histogram(cvs.T[3], bins=100, density=1)
deltabe = bin_edges[1]-bin_edges[0]
bin_edges = [bin_edges[i] + deltabe/2. for i in range(len(bin_edges)-1)]
np.savetxt('cvs_hist_a0.dat',np.array([bin_edges,histo_a0]).T,fmt="%f")

end_time = time.clock()
# timers.show(integrator.getTimers(), precision=2)

# print '\nDone.'
